package de.ma.demo;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Service
public class DatabaseLoader
{
	@Inject
	private TodoRepository todoRepository;
	
	@PostConstruct
	private void initDatabase()
	{
		if( this.todoRepository.count() == 0 )
		{
			Todo todo = new Todo( "Prepare Docker Workshop", "???" );
			this.todoRepository.save( todo );
			
			todo = new Todo( "Prepare JUG Saxony Day 2015", "???" );
			this.todoRepository.save( todo );
		}
	}
}
