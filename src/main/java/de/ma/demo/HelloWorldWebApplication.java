package de.ma.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;

@SpringBootApplication
public class HelloWorldWebApplication extends SpringBootServletInitializer
{
  @Override
  protected SpringApplicationBuilder configure(SpringApplicationBuilder application) 
  {
  	return application.sources( HelloWorldWebApplication.class );
  }
  
  public static void main(String[] args) throws InterruptedException 
  {
    SpringApplication.run( HelloWorldWebApplication.class, args );
  }
}