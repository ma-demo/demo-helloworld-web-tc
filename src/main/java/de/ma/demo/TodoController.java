package de.ma.demo;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class TodoController
{
	@Inject
	TodoRepository todoRepository;
	
	@RequestMapping( "/todos" )
	public String todoList( Model model )
	{
		Iterable<Todo> todoList = this.todoRepository.findAll();
		model.addAttribute( "todoList", todoList );
		return "todoList";
	}
}
