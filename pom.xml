<?xml version="1.0" encoding="UTF-8"?>
<project 
	xmlns="http://maven.apache.org/POM/4.0.0" 
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	
	<modelVersion>4.0.0</modelVersion>
	
	<groupId>de.ma</groupId>
	<artifactId>demo-helloworld-web-tomcat</artifactId>
	<version>1.0-dev</version>
	<packaging>war</packaging>
	
	<name>ma-demo-helloworld-web-tomcat</name>
	<description>Demo project for Dockerized Spring Boot on external Tomcat</description>
	
	<parent>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-parent</artifactId>
		<version>1.3.0.RELEASE</version>
		<relativePath/> <!-- lookup parent from repository -->
	</parent>
	
	<!-- ================================================================================================== -->
	
	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<project.build.resourceEncoding>UTF-8</project.build.resourceEncoding>
		<java.version>1.8</java.version>
		<docker-maven-plugin.version>0.4.10</docker-maven-plugin.version>
		<!-- ================================================================== -->
		<orgaName>befi</orgaName>
		<newVersion>${project.version}</newVersion>
		<forceTags>true</forceTags>
	</properties>
	
	<!-- ======================================================================================== -->
	<!--                                                                                          -->
	<!-- ======================================================================================== -->
	
	<dependencies>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-thymeleaf</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-actuator</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-data-jpa</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-configuration-processor</artifactId>
			<optional>true</optional>
		</dependency>
		
		<!-- ======================================================== -->
		
		<dependency>
			<groupId>mysql</groupId>
			<artifactId>mysql-connector-java</artifactId>
		</dependency>
		
		<dependency>
			<groupId>javax.inject</groupId>
			<artifactId>javax.inject</artifactId>
			<version>1</version>
		</dependency>
		
		<!-- ======================================================== -->

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-tomcat</artifactId>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>org.apache.tomcat.embed</groupId>
			<artifactId>tomcat-embed-jasper</artifactId>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>org.apache.tomcat.embed</groupId>
			<artifactId>tomcat-embed-core</artifactId>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>org.apache.tomcat.embed</groupId>
			<artifactId>tomcat-embed-el</artifactId>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>org.apache.tomcat</groupId>
			<artifactId>tomcat-jdbc</artifactId>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>javax.servlet</groupId>
			<artifactId>javax.servlet-api</artifactId>
			<scope>provided</scope>
		</dependency>
		
		<!-- ============================================================================ -->
		
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-test</artifactId>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.hsqldb</groupId>
			<artifactId>hsqldb</artifactId>
			<scope>runtime</scope>
		</dependency>
		
	</dependencies>
	
	<!-- ======================================================================================== -->
	<!--                                                                                          -->
	<!-- ======================================================================================== -->
	
	<build>
		<plugins>
			<!-- plugin>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-maven-plugin</artifactId>
			</plugin-->
			<!-- ====================================================== -->
			<plugin>
				<groupId>pl.project13.maven</groupId>
				<artifactId>git-commit-id-plugin</artifactId>
				<executions>
					<execution>
						<goals>
							<goal>revision</goal>
						</goals>
					</execution>
				</executions>
				<configuration>
					<dotGitDirectory>${project.basedir}/.git</dotGitDirectory>
					<dateFormat>yyyyMMdd-HHmmss</dateFormat>
					<generateGitPropertiesFile>true</generateGitPropertiesFile>
					<generateGitPropertiesFilename>target/generated-resources/git.properties</generateGitPropertiesFilename>
				</configuration>
			</plugin>
		</plugins>
	</build>
	
	<!-- ======================================================================================== -->
	<!--                                                                                          -->
	<!-- ======================================================================================== -->
	
	<profiles>
		<!-- ====================================================== -->
		<!-- buildDockerImage                                       -->
		<!-- ====================================================== -->
		<profile>
			<id>buildDockerImage</id>
			<build>
				<plugins>
					<plugin>
						<groupId>org.apache.maven.plugins</groupId>
						<artifactId>maven-resources-plugin</artifactId>
						<configuration>
							<outputDirectory>${basedir}/target/app</outputDirectory>
							<resources>
								<resource>
									<directory>src/main/docker</directory>
									<filtering>true</filtering>
								</resource>
							</resources>
						</configuration>
					</plugin>
					<plugin>
						<groupId>com.spotify</groupId>
						<artifactId>docker-maven-plugin</artifactId>
						<version>${docker-maven-plugin.version}</version>
						<configuration>
							<imageName>${orgaName}/${project.artifactId}</imageName>
							<imageTags>
								<imageTag>${newVersion}</imageTag>
							</imageTags>
							<dockerDirectory>${project.basedir}/target/app</dockerDirectory>
							<resources>
								<resource>
									<targetPath>/</targetPath>
									<directory>${project.build.directory}</directory>
									<include>${project.build.finalName}.war</include>
								</resource>
							</resources>
							<forceTags>${forceTags}</forceTags>
						</configuration>
					</plugin>
				</plugins>
			</build>
		</profile>
		<!-- ====================================================== -->
		<!-- tagDockerImage                                         -->
		<!-- ====================================================== -->
		<profile>
			<id>tagDockerImage</id>
			<build>
				<plugins>
					<plugin>
						<groupId>com.spotify</groupId>
						<artifactId>docker-maven-plugin</artifactId>
						<version>${docker-maven-plugin.version}</version>
						<configuration>
							<forceTags>${forceTags}</forceTags>
							<image>${orgaName}/${project.artifactId}:${newVersion}</image>
							<newName>${dockerRegistry}/${orgaName}/${project.artifactId}:${newVersion}</newName>
						</configuration>
					</plugin>
				</plugins>
			</build>
		</profile>
		<!-- ====================================================== -->
		<!-- pushDockerImage                                        -->
		<!-- ====================================================== -->
		<profile>
			<id>pushDockerImage</id>
			<build>
				<plugins>
					<plugin>
						<groupId>com.spotify</groupId>
						<artifactId>docker-maven-plugin</artifactId>
						<version>${docker-maven-plugin.version}</version>
						<configuration>
							<imageName>${dockerRegistry}/${orgaName}/${project.artifactId}:${newVersion}</imageName>
						</configuration>
					</plugin>
				</plugins>
			</build>
		</profile>
		
	</profiles>
</project>